<?php

namespace App\Infrastructure\Controller;

use App\Domain\Dto\NewBookDto;
use App\Domain\Service\BookService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/book')]
class BookController extends AbstractController
{
    #[Route(methods:['GET'])]
    public function getAll(BookService $bookService): Response
    {
        return $this->json($bookService->listAllBooks());
    }
    #[Route(methods:['POST'])]
    public function add(BookService $bookService, Request $request): Response
    {  
        $body = json_decode($request->getContent(), true);
        return $this->json($bookService->addNewBook(new NewBookDto($body['isbn'], $body['title'], $body['author'])));
    }
}
