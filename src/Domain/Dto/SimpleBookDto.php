<?php

namespace App\Domain\Dto;

class SimpleBookDto {


    public function __construct(
        public string $title,
        public string $author,
        public string $isbn

    ) {
    }
}