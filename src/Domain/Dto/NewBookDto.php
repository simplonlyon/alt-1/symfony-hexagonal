<?php

namespace App\Domain\Dto;


class NewBookDto {
    public function __construct(
        public string $isbn,
        public string $title,
        public string $author,
        public int $copies = 1,
        public int $pageNumber = 0
    ) {
        
    }
}