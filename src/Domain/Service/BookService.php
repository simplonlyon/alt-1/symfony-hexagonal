<?php

namespace App\Domain\Service;

use App\Domain\Dto\NewBookDto;
use App\Domain\Dto\SimpleBookDto;
use App\Domain\Entity\Book;
use App\Domain\Repository\IBookRepository;

class BookService {

    public function __construct(private IBookRepository $repo) {
        
    }

    public function listAllBooks():array {
        return array_map(
            fn(Book $item) => 
                new SimpleBookDto($item->getTitle(), $item->getAuthor(), $item->getIsbn())
                , $this->repo->findAll()
            );
    }

    public function addNewBook(NewBookDto $newBookDto):SimpleBookDto {
        $book = new Book($newBookDto->isbn, $newBookDto->title, $newBookDto->author, $newBookDto->copies, $newBookDto->pageNumber);
        $this->repo->save($book);
        return new SimpleBookDto($book->getTitle(), $book->getAuthor(), $book->getIsbn());
    }

}